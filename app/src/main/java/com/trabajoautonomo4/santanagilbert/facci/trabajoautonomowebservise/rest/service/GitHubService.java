package com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.rest.service;

import com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.rest.constant.ApiConstants;
import com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.rest.model.Followers;
import com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.rest.model.Owner;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GitHubService {

    @GET(ApiConstants.GITHUB_USER_ENDPOINT)
    Call<Owner> getOwner(@Path("owner") String owner);

    @GET(ApiConstants.GITHUB_FOLLOWERS_ENDPOINT)
    Call<List<Followers>> getOwnerFollowers(@Path("owner") String owner); // Cambio de tipo de lista Owner a Followers
}