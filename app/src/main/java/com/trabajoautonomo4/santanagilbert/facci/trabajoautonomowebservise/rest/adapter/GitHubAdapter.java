package com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.rest.adapter;

import com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.rest.constant.ApiConstants;
import com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.rest.model.Followers;
import com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.rest.model.Owner;
import com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.rest.service.GitHubService;

import java.util.List;
import retrofit2.Call;

public class GitHubAdapter extends BaseAdapter implements GitHubService {

    private GitHubService gitHubService;

    public GitHubAdapter() {
        super(ApiConstants.BASE_GITHUB_URL);
        gitHubService = createService(GitHubService.class);
    }

    @Override
    public Call<Owner> getOwner(String owner) {
        return gitHubService.getOwner(owner);
    }

    @Override
    // este metodo permite obtener la lista de seguidores del propietario
    public Call<List<Followers>> getOwnerFollowers(String owner) {
        return gitHubService.getOwnerFollowers(owner);
    }
}


