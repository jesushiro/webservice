package com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.ui.enter;

import android.app.ProgressDialog;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.R;
import com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.rest.adapter.GitHubAdapter;
import com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.rest.model.Followers;
import com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.rest.model.Owner;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoUserActivity extends AppCompatActivity {
    ArrayList<Owner> listaFollowers;
    RecyclerView recyclerViewFollowers;
    TextView textViewRepositories, textViewFollowing;
    ImageView imageViewProfile;
    ProgressDialog progress1;  // Cuadro de espera 1 datos del propietario
    ProgressDialog progress2;  // Cuadro de espera 2 datos de los seguidores


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_user);

        textViewFollowing = findViewById(R.id.textViewFollowing);
        textViewRepositories = findViewById(R.id.textViewRepositories);
        imageViewProfile = (ImageView) findViewById(R.id.imageViewProfile);

        listaFollowers = new ArrayList<>();
        recyclerViewFollowers = (RecyclerView)findViewById(R.id.recyclerViewFollowers);
        recyclerViewFollowers.setLayoutManager(new LinearLayoutManager(this));
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle( "PROPIETARIO Y SEGUIDORES" );

        String loginName = getIntent().getStringExtra("loginName");

        mostrarDatosBasicos(loginName);
    }

    private void mostrarDatosBasicos(String loginName){
        // se inicia el cuadro de dialogo para la espera de datos del propietario
        progress1 = new ProgressDialog(this);
        progress1.setMessage("Buscando Propietario");
        progress1.show();

        GitHubAdapter adapter = new GitHubAdapter();
        Call<Owner> call = adapter.getOwner(loginName);
        call.enqueue(new Callback<Owner>() {

            @Override
            public void onResponse(Call<Owner> call, Response<Owner> response) {
                progress1.dismiss();
                Owner owner = response.body();
                //condicion si encuentra al propietario
                if (owner != null) {
                    // si lo encuentra envia el numero de los repositores y los seguidores

                    // Se envia el numero de repositorios publicos al textview
                    textViewRepositories.setText(owner.getPublicRepos().toString());
                    // Se envia el numero de personas que sigue el usuario a un textview
                    textViewFollowing.setText(owner.getFollowing().toString());
                    // Se envia la imagen del usuario a un imageview
                    Picasso.get().load(owner.getAvatarUrl()).into(imageViewProfile);
                }else {
                    //caso contrario mostrar un mensaje de error
                    Toast.makeText(InfoUserActivity.this, "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Owner> call, Throwable t) {
                // no se econtro el propietario
                Toast.makeText(InfoUserActivity.this, "Ha ocurrido un error al realizar la petición", Toast.LENGTH_SHORT).show();
            }
        });
        // se inicia el cuadro de dialogo para la espera de seguidores
        progress2 = new ProgressDialog(this);
        progress2.setMessage("Buscando seguidores");
        progress2.show();

        // se cra una instancia para el llamado de las lita de seguidores
        Call<List<Followers>> callFollowers = new GitHubAdapter().getOwnerFollowers(loginName);
        callFollowers.enqueue(new Callback<List<Followers>>() {
            @Override
            public void onResponse(Call<List<Followers>> call, Response<List<Followers>> response) {
                progress2.cancel();
                // se crea una instancia de la lista recibida
                List<Followers> list = response.body();
                //si se encuentan los seguidores
                if (list != null) {
                    //se crea una intancia de la clase adapter
                    AdaptadorFollowers adapter = new AdaptadorFollowers(list);
                    // se añade al recyclerview
                    recyclerViewFollowers.setAdapter(adapter);
                } else {
                    //caso contrario mostrar un mensaje de error
                    Toast.makeText(InfoUserActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<List<Followers>> call, Throwable t) {
                //caso contrario mostrar un mensaje de error
                Toast.makeText(InfoUserActivity.this, "Ha ocurrido un error al realizar la petición", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
