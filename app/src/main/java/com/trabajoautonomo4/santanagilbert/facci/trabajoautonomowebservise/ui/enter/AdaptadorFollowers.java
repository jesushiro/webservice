package com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.ui.enter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.R;
import com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.rest.model.Followers;

import java.util.List;

public class AdaptadorFollowers extends RecyclerView.Adapter<AdaptadorFollowers.ViewHolderFollowers> {

    List<Followers> listaFollowers;
    Context context;

    // Metodo constructor que asigna los datos obtenidos a una variable
    public AdaptadorFollowers(List<Followers> listaFollowers) {
        this.listaFollowers = listaFollowers;
    }

    @NonNull
    @Override
    public ViewHolderFollowers onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        // variable donde se obtiene el contexto de la aplicación
        context = parent.getContext();
        return new ViewHolderFollowers(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderFollowers holder, int position) {
        //agregar el nombre del seguidor a item del recyclerview
        holder.etiNombre.setText(listaFollowers.get(position).getLogin());
        //agregar la imagen del seguidor a item del recyclerview
        Picasso.get().load(listaFollowers.get(position).getAvatarUrl()).into(holder.imagen);
    }

    @Override
    public int getItemCount() {
        return listaFollowers.size();
    }

    public class ViewHolderFollowers extends RecyclerView.ViewHolder {

        TextView etiNombre;
        ImageView imagen;

        public ViewHolderFollowers(View itemView) {
            super(itemView);

            etiNombre = (TextView) itemView.findViewById(R.id.textViewLista);
            imagen = (ImageView) itemView.findViewById(R.id.imageViewLista);

        }
    }
}