package com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.ui.enter;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.trabajoautonomo4.santanagilbert.facci.trabajoautonomowebservise.R;

public class EnterUserActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etUser;
    private Button btnFollowers;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_user);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle( "INGRESO DE OWNER" );
        initViews();
    }
    private void initViews() {
        etUser = findViewById(R.id.enter_user_edit_text);
        btnFollowers = findViewById(R.id.enter_user_button);
        btnFollowers.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btnFollowers) {
            //se crea una instancia intent para trasladarse a otra activity o enviar información
            Intent intent = new Intent(this, InfoUserActivity.class);
            // del metodo putEstra se envia el nombre escrito en el EditText
            intent.putExtra("loginName", etUser.getText().toString().trim());
            //traslado de activity
            startActivity(intent);
        }
        etUser.setText( "" );
    }
}
